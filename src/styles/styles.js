import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    backgroundColor: "#ffffff"
  },
  button: {
    marginBottom: 10,
    marginTop: 10
  },
  warning: {
    color: "red"
  },
  screenContainer: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    padding: 20
  },
  playerContainer: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "stretch"
  },
  playerTitle: {
    fontWeight: "bold",
    fontSize: 18
  },
  podcastInfoImage: {
    width: 300,
    height: 300
  }
});

export { styles };
