import React from "react";
import { Image } from "react-native";
import { styles } from "podtest/src/styles";
import { Text } from "podtest/src/components";

const PlayerInfo = props => {
  return (
    <React.Fragment>
      <Image source={{ uri: props.image }} style={styles.podcastInfoImage} />
      <Text style={styles.playerTitle}>{props.podcast_title}</Text>
      <Text>{props.title}</Text>
    </React.Fragment>
  );
};

export default PlayerInfo;
