import React from "react";
import { ActivityIndicator, View } from "react-native";
import TrackPlayer from "react-native-track-player";
import { styles } from "podtest/src/styles";
import { Text } from "podtest/src/components";
import PlayerInfo from "./PlayerInfo";
import PlayerStatusBar from "./PlayerStatusBar";
import PlayerControls from "./PlayerControls";

export default class PodcastPlayer extends React.Component {
  static LOADING_ERROR = "This file cannot be loaded, try again later";
  state = {
    status: null,
    error: null
  };

  componentWillUnmount() {
    TrackPlayer.stop();
    this.onStateChange.remove();
    TrackPlayer.destroy();
  }

  async componentWillMount() {
    const { podcast_title, audio, title, image } = this.props.podcast;

    /*******
     * no need to go further if audio doesn't exist
     * the api call may be a better place for this
     */

    if (!audio) {
      this.showError(PodcastPlayer.LOADING_ERROR);
    }

    TrackPlayer.setVolume(0.1);

    /****************
     * This is a hacky way of handling status & errors.  I would do a deeper dive
     * on this audio component to understand its events
     */

    this.onStateChange = TrackPlayer.addEventListener("playback-state", async data => {
      this.setState({
        status: data.state
      });
    });

    await TrackPlayer.add({
      url: audio,
      title: title,
      artist: podcast_title,
      artwork: image
    });

    TrackPlayer.play();
  }

  render() {
    const { podcast_title, title, image } = this.props.podcast;

    if (this.state.error) {
      return (
        <React.Fragment>
          <Text style={styles.warning}>{this.state.error}</Text>
          <PlayerControls onClose={this.props.onClose} />
        </React.Fragment>
      );
    }
    if (this.state.status !== TrackPlayer.STATE_PLAYING && this.state.status !== TrackPlayer.STATE_PAUSED) {
      return <ActivityIndicator size="large" />;
    }
    return (
      <View style={styles.playerContainer}>
        <PlayerInfo image={image} podcast_title={podcast_title} title={title} />
        <PlayerStatusBar onValueChange={this.changePosition} />
        <PlayerControls status={this.state.status} onPause={this.onPause} onPlay={this.onPlay} onClose={this.props.onClose} />
      </View>
    );
  }

  changePosition = position => {
    TrackPlayer.seekTo(position);
    TrackPlayer.play();
  };

  onPlay = () => {
    TrackPlayer.play();
  };

  onPause = () => {
    TrackPlayer.pause();
  };

  showError = errorMsg => {
    this.setState({
      error: errorMsg
    });
  };
}
