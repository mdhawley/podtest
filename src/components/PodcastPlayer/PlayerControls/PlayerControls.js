import React from "react";
import TrackPlayer from "react-native-track-player";
import { Button } from "podtest/src/components";
import { styles } from "podtest/src/styles";

export default class PlayerControls extends TrackPlayer.ProgressComponent {
  render() {
    let actionButton = this.getActionButton();
    return (
      <React.Fragment>
        {actionButton}
        <Button style={styles.button} title="close" onPress={this.props.onClose} />
      </React.Fragment>
    );
  }

  getActionButton = () => {
    switch (this.props.status) {
      case TrackPlayer.STATE_PLAYING:
        return <Button style={styles.button} title="pause" onPress={this.props.onPause} />;
      case TrackPlayer.STATE_PAUSED:
        return <Button style={styles.button} title="play" onPress={this.props.onPlay} />;
      default:
        return null;
    }
  };
}
