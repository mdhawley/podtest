import React from "react";
import TrackPlayer from "react-native-track-player";
import { View } from "react-native";
import { Text } from "podtest/src/components";
import { Slider } from "react-native-elements";

export default class PlayerStatusBar extends TrackPlayer.ProgressComponent {
  render() {
    return (
      <View>
        <Slider value={this.getProgress()} onValueChange={value => this.props.onValueChange(this.state.duration * value)} />
        <Text>{this.state.position}</Text>
        <Text>{this.state.bufferedPosition}</Text>
      </View>
    );
  }
}
