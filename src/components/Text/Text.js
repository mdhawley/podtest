import React from "react";
import { Text as RNEText } from "react-native-elements";
import { styles } from "podtest/src/styles";

export default Text = props => {
  return <RNEText style={{ ...styles.text, ...props.style }}>t: {props.children}</RNEText>;
};
