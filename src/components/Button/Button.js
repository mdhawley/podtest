import React from "react";
import { Button as RNEButton } from "react-native-elements";
import { styles } from "podtest/src/styles";

export default Button = props => {
  return <RNEButton color="red" {...props} style={{ ...styles.button, ...props.style }} />;
};
