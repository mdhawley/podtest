import React from "react";
import { View } from "react-native";
import SelectPodcast from "./SelectPodcast";
import { PodcastPlayer } from "podtest/src/components";
import { styles } from "podtest/src/styles";

export default class Podcast extends React.Component {
  state = {
    selectedPodcast: null
  };

  render() {
    return (
      <View style={styles.screenContainer}>
        {this.state.selectedPodcast ? (
          <PodcastPlayer onClose={this.onClosePodcast} podcast={this.state.selectedPodcast} />
        ) : (
          <SelectPodcast onPodcastSelected={this.onPodcastSelected} />
        )}
      </View>
    );
  }

  onPodcastSelected = selectedPodcast => {
    this.setState({
      selectedPodcast
    });
  };

  onClosePodcast = () => {
    this.setState({
      selectedPodcast: null
    });
  };
}
