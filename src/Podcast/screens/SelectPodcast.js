import React from "react";
import { ActivityIndicator } from "react-native";
import { getRandomPodcast } from "podtest/src/api";
import { styles } from "podtest/src/styles";
import { Button, Text } from "podtest/src/components";

export default class SelectPodcast extends React.Component {
  state = {
    error: false,
    loading: false
  };

  render() {
    if (this.state.loading) {
      return <ActivityIndicator />;
    }

    return (
      <React.Fragment>
        <Button title="Play Random Podcast" solid onPress={this.onGetPodcastPressed} />
        {this.state.error && <Text style={styles.warning}>There was an error</Text>}
      </React.Fragment>
    );
  }

  onGetPodcastPressed = async () => {
    this.setState({
      loading: true
    });
    const result = await getRandomPodcast();

    if (result.status === 200) {
      this.props.onPodcastSelected(result.data);
    } else {
      this.setState({
        error: true
      });
    }
  };
}
