/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import TrackPlayer from "react-native-track-player";
import { Podcast } from "podtest/src/Podcast";
import { styles } from "podtest/src/styles";

type Props = {};
export default class App extends React.Component<Props> {
  componentWillMount() {
    TrackPlayer.setupPlayer().then(() => {});
    TrackPlayer.registerPlaybackService(() => require("./service.js"));
  }

  render() {
    return (
      <SafeAreaView style={styles.appContainer}>
        <ScrollView>
          <Podcast />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
