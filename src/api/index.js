import { LISTEN_API_KEY, LISTEN_API_URL } from "./constants";
import axios from "axios";

const getRandomPodcast = async () => {
  try {
    const instance = getListenInstance();
    const response = await instance.get("/just_listen");
    return response;
  } catch (error) {
    return error.response;
  }
};

const getListenInstance = () => {
  return axios.create({
    baseURL: LISTEN_API_URL,
    headers: { "X-ListenAPI-Key": LISTEN_API_KEY }
  });
};

export { getRandomPodcast };
