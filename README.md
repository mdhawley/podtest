# podtest

# Requirements

- NPM
- Node 8+
- Xcode
- react-native-cli

# Getting up and running

```sh
$ cd <YOUR_WORKSPACE>
$ git clone https://github.com/mdhawley/podtest
$ cd ./podtest
$ yarn
$ react-native link
$ yarn run ios
```
